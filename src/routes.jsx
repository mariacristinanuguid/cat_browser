var React = require('react');
var Router = require('react-router').Router;
var Route = require('react-router').Route;

var Cat = require('./Components/Cat');
var CatDetails = require('./Components/CatDetails');

module.exports = (
    <Router>
        <Route path="/" component={Cat}>
        </Route>
        <Route path="/{id}" component={CatDetails}>
        </Route>
    </Router>
);