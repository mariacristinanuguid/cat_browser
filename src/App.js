import './App.css';
import Cat from './Components/Home';
import CatDetails  from './Components/CatDetails';
import {
		BrowserRouter,
		Switch,
		Route
} from "react-router-dom";

function App() {
  return (
	<div className="App">
		<BrowserRouter>
			<Switch>
				<Route exact path="/" component={Cat}></Route>
				<Route path="/:id" component={CatDetails}></Route>
			</Switch>
      </BrowserRouter>
	</div>
  );
}

export default App;
