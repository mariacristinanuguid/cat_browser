import { Button, Card } from 'react-bootstrap';
import React, { Component } from 'react';

class Cat extends Component{    
    render(){
        const { cats } = this.props;
        
        if(cats.length <= 0){
            return (
                <div className="col-12" style={{ marginBottom: "20px"}}>No cats available</div>
            );
        }else{
            
            return (
                cats.map((cat, idx) =>
                    <div className="col-md-3 col-sm-6 col-12" key={idx}>
                        <Card>
                            <Card.Img variant="top" src={cat.url} alt="cat" />
                            <Card.Body>
                                <Button href={"/"+cat.id} variant="primary" style={{width: "100%"}}>View details</Button>
                            </Card.Body>
                        </Card>
                    </div>
                )
            );
        }
    }

  }
  
  export default Cat;