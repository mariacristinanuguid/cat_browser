import React, { Component } from 'react';
import Card from'./Cat';
import axios from 'axios';
import { Form, Button } from 'react-bootstrap';

const baseURL = 'https://api.thecatapi.com/v1';

class Home extends Component {
    constructor(props) {
        super(props);
        this.handleBreedChange = this.handleBreedChange.bind(this);
        this.state = {breed: null, breeds: [], page: 1, isDisabled: true, isLoading: false, cats: [], prevSize:0, isHidden:false, length: 0};
    }
    handleBreedChange = (event) =>{
        event.target.value ? this.setState({isDisabled: false}) : this.setState({isDisabled: true});
        this.setState({breed: event.target.value, page: 1, isDisabled: true, isLoading: false, cats: [],prevSize:0,isHidden:false});
    }
    loadMore = () =>{
        this.setState((prevState) =>({
            page: prevState.page + 1
        }));
    }
    componentDidMount = () =>{
        axios.get(baseURL + '/breeds')
        .then(response => {
            const breeds = response.data;
            this.setState({ breeds});

            var search = this.props.location.search;
            var name = new URLSearchParams(search).get("breed");

            if(name != null){
                this.setState({breed: name});
                this.loadCats();
            }
        });
    }
    componentDidMount() {
        this.loadCats();
    }
    componentDidUpdate(prevProps, prevState) {
        
        if (prevState.page !== this.state.page) {
            this.loadCats();
        }

        if(prevState.breed !== this.state.breed){
            this.loadCats();
        }
    }
    loadCats = () =>{
        const { breed, page } = this.state;
        this.setState({ isLoading: true });

        if(!breed){
            this.setState({cats: [], isLoading: false});
            return;
        }

        axios.get(`${baseURL}/images/search?page=${page}&limit=10&breed_id=${breed}`).then((response) => {
                       
            this.setState((prevState) => ({
                prevSize: prevState.cats.length,
                cats: [...prevState.cats, ...response.data]
            }));

            this.setState((prevState) =>{
                const totalRows = prevState.cats.filter(x => !response.data.includes(x)).concat(response.data.filter(x => !prevState.cats.includes(x))).length;

                if(prevState.length === totalRows && totalRows > 0){
                    return {isHidden: true};
                }else{
                    return {length: totalRows};
                }
            });

            this.setState({cats: this.state.cats.filter((v,i) => {
                    return this.state.cats.map((val)=> val.id).indexOf(v.id) === i
                }),
                isDisabled: false
            });

        }).finally(() => {
            this.setState({ isLoading: false });
        });
    }
    render(){

        const {breeds, isLoading, isDisabled, cats,isHidden} = this.state;

        return (
            <div className="Home">
                <div className="container">
                    <h1>Cat Browser</h1>
                    <div className="row" style={{ padding: "10px 0px" }}>
                        <div className="col-md-3 col-sm-6 col-12">
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Breed</Form.Label>
                                <Form.Select aria-label="Select Breed" id="breed" onChange={this.handleBreedChange} value={this.state.breed ?? ""}>
                                    <option value="">Select Breed</option>
                                    { breeds.map(breed => <option key={breed.id} value={breed.id}>{breed.name}</option>)}
                                </Form.Select>
                            </Form.Group>
                        </div>
                    </div>
                    <div className="row">
                        <Card cats={cats}></Card>
                    </div>
                    <div className="row">
                        <div className="col-md-3 col-sm-6 col-12">
                            <Button variant="primary" style={{ visibility: isHidden ? 'hidden': 'visible'}} className="btn btn-success" disabled={isDisabled} onClick={this.loadMore}>{isLoading ? 'Loading Cat...' : 'Load More'}</Button>{' '}
                        </div>
                    </div>
                </div>
            </div>
            
        );
    }
}

export default Home;