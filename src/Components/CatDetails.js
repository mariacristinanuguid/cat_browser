import { Button, Card, Image} from 'react-bootstrap';
import React, { Component } from 'react';
import axios from 'axios';

const baseURL = 'https://api.thecatapi.com/v1/images';

class CatDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {details: null,isLoading:true};
    }

    componentDidMount = () =>{
        axios.get(baseURL + '/' + this.props.match.params.id).then(response => {
            const details = response.data
            this.setState({details:details});
        }).finally(() => {
            this.setState({ isLoading: false });
        });
    }

    render(){
        const { details,isLoading } = this.state;
        return (
            
            <div className="container">
                {isLoading ? 'LOADING' : 
                <Card>
                    <Card.Header style={{ textAlign: 'left' }}>
                        <Button href={`/?breed=${details.breeds[0].id}`}>Back</Button>
                    </Card.Header>
                    <Image src={details.url} alt="card-img" />
                    <Card.Body style={{ textAlign: 'left' }}>
                        <Card.Title>{details.breeds[0].name}</Card.Title>
                        <Card.Text as="h5">
                            Origin: {details.breeds[0].origin}
                        </Card.Text>
                        <Card.Text as="h6">
                            {details.breeds[0].temperament}
                        </Card.Text>
                        <Card.Text>
                            {details.breeds[0].description}
                        </Card.Text>
                    </Card.Body>
                </Card>
            
            }
            </div>
        )
    }
}

export default CatDetails;